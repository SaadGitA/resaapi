import logging
from datetime import timedelta

from flask import jsonify, request
from flask_restplus import Resource, Namespace, abort
from flasgger.utils import swag_from
from marshmallow import Schema, fields, ValidationError, validates_schema
from flask_jwt_extended import create_access_token
from flaskapp.queries.queries import get_users, add_user, check_user_exists, delete_user, get_one_user, check_user_password


users_ns = Namespace('users', description='Users management')

LOGGER = logging.getLogger(__name__)

# New user validation class


class UsersPostSchema(Schema):
    first_name = fields.String(required=True)
    last_name = fields.String(required=True)
    password = fields.String(required=True)
    # phone en string car sinon il ne prend pas le 0
    phone = fields.String(required=True)
    mail = fields.String(required=True)
    bandname = fields.String(required=True)

    @validates_schema
    #pylint: disable=unused-argument
    def validate_schema_parameters(self, data, **kwargs):
        if len(data['password']) < 5:
            raise ValidationError(message='Password must be 5 characters')
        if len(data['phone']) != 10:
            raise ValidationError(message='Phone must be 10 numbers')
        if '@' not in data['mail']:
            raise ValidationError(message='No @ found in mail adress')
        mail_extension = ('.fr', '.com')
        if not data['mail'].endswith(mail_extension):
            raise ValidationError(
                message='No .fr or .com found in mail adress')

# Check authentification user class


class UserPostSchema(Schema):
    password = fields.String(required=True)


@users_ns.route("/")
class Users(Resource):
    @swag_from('specs/get.yml')
    def get(self):
        return jsonify(get_users())

    @swag_from('specs/post.yml')
    def post(self):
        try:
            userinfos = UsersPostSchema().load(request.values)
        except ValidationError as error:
            abort(400, message=error.messages)

        # Check si le mail n'existe pas
        if not check_user_exists(userinfos['mail']):
            add_user(userinfos)
        else:
            abort(400, message='Mail {0} already exists'.format(
                userinfos['mail']))

        return "User {0} added succssfully ! ".format(userinfos['mail'])


@users_ns.route("/<string:usermail>")
class UsersUpdate(Resource):
    @swag_from('specs/user/delete.yml')
    def delete(self, usermail):
        if not check_user_exists(usermail):
            abort(400, message='Mail {0} not found'.format(usermail))
        else:
            delete_user(usermail)

        return "User {0} deleted succssfully ! ".format(usermail)

    # Get one user info
    @swag_from('specs/user/get.yml')
    def get(self, usermail):
        if check_user_exists(usermail):
            return jsonify(get_one_user(usermail))
        return abort(400, message='Mail {0} not found'.format(usermail))

    # Check if password is OK then return a token
    @swag_from('specs/user/post.yml')
    def post(self, usermail):
        try:
            userinfos = UserPostSchema().load(request.values)
        except ValidationError as error:
            abort(400, message=error.messages)

        if check_user_exists(usermail):
            if not check_user_password(usermail, userinfos['password']):
                abort(400, message='Wrong password for {0}'.format(usermail))
        else:
            abort(400, message='Mail {0} not found'.format(usermail))

        return {'access_token': create_access_token(identity={'username': usermail}, expires_delta=timedelta(days=999999))}, 200
