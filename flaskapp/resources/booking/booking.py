import logging

from dateutil.parser import parse
from flask import jsonify, request
from flask_restplus import Resource, Namespace, abort
from flasgger.utils import swag_from
from marshmallow import Schema, fields, ValidationError, validates_schema
from flaskapp.queries.queries import check_same_day, check_hour_interval, check_user_exists, get_hour_from_date, get_all_end_date, add_booking, get_date_format, check_booking_exists, get_bookings_by_id, check_late_booking_exists, delete_booking, get_all_bookings, validate_start_date
from utils.utils import validate_hour_format

booking_ns = Namespace('booking', description='Booking management')

LOGGER = logging.getLogger(__name__)

# New Booking validation class


class BookingPostSchema(Schema):
    start_date = fields.String(required=True)
    start_hour = fields.String(required=True)
    end_date = fields.String(required=True)
    end_hour = fields.String(required=True)
    mail = fields.String(required=True)
    room_id = fields.Integer(required=True)

    @validates_schema
    #pylint: disable=unused-argument
    def validate_schema_parameters(self, data, **kwargs):
        # check that start day is today or later
        start_date_validation = validate_start_date(data['start_date'])

        if start_date_validation < 0:
            raise ValidationError(
                message='Start date must start at least today')

        # si négatif on renvoie une erreur
        date_diff = check_same_day(data['end_date'], data['start_date'])
        if date_diff < 0:
            raise ValidationError(
                message='End date must be after start date !')
        if date_diff > 1:
            raise ValidationError(message='Booking can only be for one day !')
        # cas ou on finit a 1heure du matin
        if int(data['start_hour'][0:2]) in range(2, 7):
            raise ValidationError(
                message='Bookings are not allowed between 1am and 7am !')

        if int(data['end_hour'][0:2]) in range(2, 7):
            raise ValidationError(
                message='Bookings are not allowed between 1am and 7am !')

        start_hour_validation_msg = validate_hour_format(data['start_hour'])
        end_hour_validation_msg = validate_hour_format(data['end_hour'])

        if start_hour_validation_msg != "OK":
            abort(400, message=start_hour_validation_msg)
        if end_hour_validation_msg != "OK":
            abort(400, message=end_hour_validation_msg)

        booking_start_date = data['start_date'] + " " + \
            data['start_hour'][0:2] + ":" + data['start_hour'][2:4]
        booking_end_date = data['end_date'] + " " + \
            data['end_hour'][0:2] + ":" + data['end_hour'][2:4]

        # 2H booking minimum
        hour_interval = check_hour_interval(
            booking_end_date, booking_start_date)
        if int(hour_interval) < 0:
            raise ValidationError(
                message='Booking end date must be after start date !')
        if int(hour_interval) <= 1:
            raise ValidationError(message='Must be a 2H booking minimum !')

        # check que le user exist
        if not check_user_exists(data['mail']):
            raise ValidationError(
                message='Mail {0} not found'.format(data['mail']))

        # validation que le créneau est dispo (check que start_date n'est pas dans un intervale de creneau)
        booking_dates = get_all_end_date(
            get_date_format(booking_start_date), data['room_id'])

        if booking_dates:
            for booking_date in booking_dates:
                if int(get_hour_from_date(booking_start_date)) in range(int(booking_date['start_hour']), int(booking_date['end_hour'])):
                    raise ValidationError(
                        message='Booking date is not available')

                if check_booking_exists(booking_start_date, data['room_id']):
                    raise ValidationError(
                        message='Booking date is not available')

        # case when late booking exists
        # Check that end booking hour is before starting hour
        late_booking_infos = check_late_booking_exists(
            data['start_date'], data['room_id'])
        if late_booking_infos:
            if int(data['end_hour']) > int(late_booking_infos['start_date'].hour):
                raise ValidationError(message='Booking date is not available')


class BookingGetSchema(Schema):
    booking_date = fields.String(required=True)
    start_hour = fields.String(required=True)


@booking_ns.route("/")
class Booking(Resource):
    @swag_from('specs/get.yml')
    def get(self):
        return jsonify(get_all_bookings())

    @swag_from('specs/post.yml')
    def post(self):
        try:
            bookinginfos = BookingPostSchema().load(request.values)
        except ValidationError as error:
            abort(400, message=error.messages)

        bookinginfos['booking_start_date'] = bookinginfos['start_date'] + " " + \
            bookinginfos['start_hour'][0:2] + ":" + \
            bookinginfos['start_hour'][2:4]
        bookinginfos['booking_end_date'] = bookinginfos['end_date'] + " " + \
            bookinginfos['end_hour'][0:2] + ":" + bookinginfos['end_hour'][2:4]
        add_booking(bookinginfos)

        return jsonify(bookinginfos)


@booking_ns.route("/<string:booking_date>/<string:start_hour>/<int:room_id>")
class BookingUpdate(Resource):
    @swag_from('specs/booking/get.yml')
    def get(self, booking_date, start_hour, room_id):
        # Check date format
        try:
            parse(booking_date)
        except ValueError:
            abort(400, message="Date must be YYYY-MM-DD. Example 2020-04-11")

        if len(booking_date) != 10:
            abort(400, message="Date must be YYYY-MM-DD. Example 2020-04-11")

        hour_validation_msg = validate_hour_format(start_hour)
        if hour_validation_msg != "OK":
            abort(400, message=hour_validation_msg)

        booking_datetime = booking_date + " " + \
            start_hour[0:2] + ":" + start_hour[2:4]
        booking_data = check_booking_exists(booking_datetime, room_id)

        # if a booking exists
        if booking_data:
            if len(booking_data) > 1:
                abort(400, message="Error: Multiple bookings found for booking date {}".format(
                    booking_datetime))
            else:
                return jsonify(get_bookings_by_id(booking_data[0]['id']))
        else:
            abort(400, message="No booking found for date {}".format(
                booking_datetime))
        return 'No booking found'

    @swag_from('specs/put.yml')
    def put(self, booking_id):
        return booking_id

    @swag_from('specs/booking/delete.yml')
    def delete(self, booking_date, start_hour, room_id):
        booking_start_date = booking_date + " " + \
            start_hour[0:2] + ":" + start_hour[2:4]
        booking_data = check_booking_exists(booking_start_date, room_id)
        LOGGER.warning(booking_data)
        if not booking_data:
            abort(400, message='Booking date {0} not found'.format(
                booking_start_date))

        start_hour_validation_msg = validate_hour_format(start_hour)
        if start_hour_validation_msg != "OK":
            abort(400, message=start_hour_validation_msg)

        delete_booking(booking_data[0]['id'])

        return "Booking deleted"
