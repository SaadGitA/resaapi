# encoding: utf-8
import logging
from contextlib import contextmanager, closing
from mysql.connector import connect
from mysql.connector.errors import Error as MySQLError
from config.config import Config
from config.singleton import singleton


@singleton
class BookingDB():

    def __init__(self):
        self.last_inserted_id = None
        self.config_db = Config.get_config()
        self.db_name = self.config_db["BOOKING_DB"]["DB_NAME"]
        self.host = self.config_db["BOOKING_DB"]["DB_HOST"]
        self.db_user = self.config_db["BOOKING_DB"]["DB_USER"]
        self.passwd = self.config_db["BOOKING_DB"]["DB_PASSWORD"]
        self.port = self.config_db["BOOKING_DB"]["DB_PORT"]
        self.timeout = self.config_db["BOOKING_DB"]["TIMEOUT"] if "TIMEOUT" in self.config_db else None

    @contextmanager
    def connect(self):
        """
            Etablit la connexion à la base de données, et renvoie la connexion
        """
        dbcon = connect()  # no params => does not attempt connection automatically, only creates a MySQLConnection
        with closing(dbcon):
            try:
                # cf. https://github.com/mysql/mysql-connector-python/blob/master/lib/mysql/connector/connection.py#L98
                # pylint: disable=protected-access
                # in seconds, passed to socket.socket.settimeout
                dbcon._connection_timeout = self.timeout
                dbcon.connect(host=self.host, user=self.db_user, passwd=self.passwd, db=self.db_name,
                              port=self.port)
                dbcon.sql_mode = ''
                yield dbcon
                dbcon.commit()
            except MySQLError as error:
                print(error)
                dbcon.rollback()
                raise

    def fetchall(self, query, parameters=()):
        """
            Renvoie toutes les lignes issues d'une requête
        """
        # pylint: disable=broad-except
        try:
            with self.connect() as dbcon:
                cursor = dbcon.cursor(dictionary=True, buffered=True)
                with closing(cursor):
                    cursor.execute(query, parameters)
                    return cursor.fetchall()
        except MySQLError as error:
            print('MySQL fetchall query error {}'.format(error))
        finally:
            if cursor:
                cursor.close()

    def fetchone(self, query, parameters=()):
        """
            Renvoie la première ligne issue d'une requête
        """
        # pylint: disable=broad-except
        try:
            with self.connect() as dbcon:
                cursor = dbcon.cursor(dictionary=True, buffered=True)
                with closing(cursor):
                    cursor.execute(query, parameters)
                    return cursor.fetchone()
        except MySQLError as error:
            logging.error('MySQL execute query error %s', error)
            print('MySQL fetchone query error {}'.format(error))
        finally:
            if cursor:
                cursor.close()

    def execute(self, query, parameters=()):
        """
            Exécute une requête paramétrée
        """
        # pylint: disable=broad-except
        try:
            with self.connect() as dbcon:
                cursor = dbcon.cursor(dictionary=True, buffered=True)
                with closing(cursor):
                    result = cursor.execute(query, parameters)
                    self.last_inserted_id = cursor.lastrowid
                    return result
        except MySQLError as error:
            logging.error('MySQL execute query error %s', error)
            print('MySQL execute query error {}'.format(error))
