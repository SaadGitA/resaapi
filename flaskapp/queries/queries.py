# encoding: utf-8
import logging
from flaskapp.partners.database import BookingDB


LOGGER = logging.getLogger(__name__)

# USERS MANAGEMENT QUERIES


def get_users():
    query = "SELECT * FROM users"
    data = BookingDB().fetchall(query)
    return data


def get_one_user(usermail):
    query = "SELECT * FROM users where mail = '{0}'".format(usermail)
    data = BookingDB().fetchone(query)
    return data


def add_user(userinfos):
    query = "INSERT INTO users (first_name, mail, password, last_name, phone, bandname) VALUES "  \
            "('{0}', '{1}', MD5('{2}'), '{3}', '{4}','{5}')" \
            .format(userinfos['first_name'], userinfos['mail'], userinfos['password'], userinfos['last_name'], userinfos['phone'], userinfos['bandname'])
    BookingDB().execute(query)


def check_user_exists(mail):
    query = "SELECT 'exists' FROM (SELECT mail FROM users WHERE mail = '{0}') AS existance".format(
        mail)
    data = BookingDB().fetchone(query)
    return data is not None


def delete_user(mail):
    query = "DELETE FROM users WHERE mail = '{0}'".format(mail)
    BookingDB().execute(query)


def check_user_password(mail, password):
    query = "SELECT 'exists' FROM (SELECT * FROM users where mail = '{0}' AND password = MD5('{1}')) AS existance".format(
        mail, password)
    data = BookingDB().fetchone(query)
    return data is not None


# BOOKING MANAGEMENT QUERIES
def get_all_bookings():
    query = "SELECT * FROM bookings"
    data = BookingDB().fetchall(query)
    return data


def get_bookings(booking_date):
    query = "SELECT * FROM bookings WHERE DATE_FORMAT(start_date, '%Y-%m-%d') LIKE '{0}'".format(
        booking_date)
    data = BookingDB().fetchall(query)
    return data


def get_bookings_by_id(booking_id):
    query = "SELECT * FROM bookings WHERE id = {}".format(booking_id)
    data = BookingDB().fetchone(query)
    return data


def check_booking_exists(booking_start_date, room_id):
    query = "SELECT id, count(*) as total FROM bookings WHERE DATE_FORMAT(start_date, '%Y-%m-%d %H:%i') = '{0}' AND room_id = {1} GROUP BY id".format(
        booking_start_date, room_id)
    data = BookingDB().fetchall(query)
    return data


def check_late_booking_exists(booking_start_date, room_id):
    query = "SELECT * FROM bookings WHERE DATE_FORMAT(start_date, '%Y-%m-%d') = '{0}' AND room_id = {1} AND DATE_FORMAT(end_date, '%Y-%m-%d') = DATE_ADD('{0}', INTERVAL 1 DAY)".format(
        booking_start_date, room_id)
    data = BookingDB().fetchone(query)
    return data


def add_booking(bookinginfos):
    query = "INSERT INTO bookings (start_date, end_date, mail, room_id) VALUES "  \
            "('{0}', '{1}', '{2}', '{3}')" \
            .format(bookinginfos['booking_start_date'], bookinginfos['booking_end_date'], bookinginfos['mail'], bookinginfos['room_id'])
    BookingDB().execute(query)


def delete_booking(booking_id):
    query = "DELETE FROM bookings WHERE id={}".format(booking_id)
    BookingDB().execute(query)

# check start date is today or later


def validate_start_date(start_date):
    query = "SELECT DATEDIFF('{0}',DATE_FORMAT(NOW(),'%Y-%m-%d')) as daycheck".format(
        start_date)
    daycheck = BookingDB().fetchone(query)
    return daycheck['daycheck']

# check date diff


def check_same_day(end_date, start_date):
    query = "SELECT DATEDIFF('{0}', '{1}') as datediff".format(
        end_date, start_date)
    date_diff = BookingDB().fetchone(query)
    return date_diff['datediff']

# Check hour intervall is 2 minimum


def check_hour_interval(end_date, start_date):
    query = "SELECT TIME_FORMAT(TIMEDIFF('{0}', '{1}'), '%H') as hourdiff".format(
        end_date, start_date)
    hour_diff = BookingDB().fetchone(query)
    return hour_diff['hourdiff']


def get_hour_from_date(booking_date):
    query = "SELECT TIME_FORMAT('{0}', '%H') as hour".format(booking_date)
    hour = BookingDB().fetchone(query)
    return hour['hour']


def get_date_format(booking_date):
    query = "SELECT DATE_FORMAT('{0}', '%Y-%m-%d') as booking_date".format(
        booking_date)
    booking_date = BookingDB().fetchone(query)
    return booking_date['booking_date']


def get_all_end_date(booking_date, room_id):
    query = "SELECT TIME_FORMAT(start_date, '%H') as start_hour, TIME_FORMAT(end_date, '%H') as end_hour FROM bookings  as hour WHERE DATE_FORMAT(start_date, '%Y-%m-%d') LIKE '{0}' AND room_id={1}".format(
        booking_date, room_id)
    booking_dates = BookingDB().fetchall(query)
    return booking_dates
