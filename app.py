from flask import Flask, Blueprint
from flask_restplus import Resource, Api
from flasgger import Swagger
from flask_jwt_extended import JWTManager
from flaskapp.resources.users.users import users_ns
from flaskapp.resources.booking.booking import booking_ns


app = Flask(__name__)
api = Api(app)
Swagger(app)
app.config['JWT_SECRET_KEY'] = 'I love metalcore'
JWTManager(app)

# Add ressources


def init_flaskapp(app):
    blueprint = Blueprint('api', __name__, url_prefix='/api')
    api.init_app(blueprint)
    add_ressources()
    app.register_blueprint(blueprint)


def add_ressources():
    api.add_namespace(users_ns)
    api.add_namespace(booking_ns)


def run_app():
    init_flaskapp(app)
    app.run(host='0.0.0.0', port=5150, debug=True)


# Running app
if __name__ == '__main__':
    run_app()
