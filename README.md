# BOOKING API 

API Booking documentation.

## Project details
- This is a personnal project which consist in building an API to book music rehearsal studios.
- This project is based on Python3/Flask/MySQL


## Run the project

- Create MySQL DB/tables with the utils/create_DB.sql script
- Run the project simply run the app.py (after filling the config/config.yml with MySQL credentials)
- You can also build / run the docker image

####  Gitlab CI pipeline

- For now only linting stage is done

## Source code structure

### Project structure

- *config*: config.yml has to be filled with your own MySQL DB. Then the Config class is loaded with the informations of this file
- *partners*: For now the only partner is your database 
- *queries*: All the DB queries are here
- *resources*: Main code of the project
    - booking: Booking management
    - Users: Users management
- *utils*: Little utils functions and the database creation script
- *tests*: Will have unit tests and functionnal tests
- app.py: Run the app
