from setuptools import setup

setup(
    name='resacp',
    packages=['.'],
    include_package_data=True,
    setup_requires=['pytest-runner'],
    install_requires=[
        'flask',
        'flask_restful',
        'flask_restplus',
        'Werkzeug==0.16.0',
        'flask_jwt_extended',
        'flasgger',
        'marshmallow',
        'mysql-connector',
        'flask-mysqldb',
        'requests',
        'flask_cors',
        'PyYAML',
        'flask_login',
        'python-dateutil',
    ],
)
