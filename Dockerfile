FROM python:3.7

# Install vi
RUN apt-get update && apt-get install apt-file -y && apt-file update && apt-get install vim -y && apt-get install telnet

# Setting local timezone
ENV TZ Europe/Paris
RUN cp /usr/share/zoneinfo/Europe/Paris /etc/localtime

# Copie du projet dans docker
COPY . /app/resacp


WORKDIR /app/resacp

#Install des modules python
RUN pip install --no-cache-dir -e .

WORKDIR /app/resacp

RUN groupadd grp_dmg
RUN useradd  --shell /bin/ksh -G grp_dmg python --uid 12000

USER 12000

#HEALTHCHECK --interval=30s --timeout=30s --retries=3 CMD curl -f http://localhost:5000/api/v1/healthcheck || exit 1

EXPOSE 5150

CMD ["python", "app.py"]

# Recommended Docker labels:
LABEL name=apiresacp
ARG VERSION
LABEL version=$VERSION
