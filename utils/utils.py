def validate_hour_format(hour):
    # Hour is string so it is easy to call it in request path
    # check hour
    if int(hour[0]) not in range(0, 3):
        return "{} must be 0,1,2".format(int(hour[0]))

    if int(hour[1]) not in range(0, 10):
        return "{} must be in range 0-9".format(hour[1])

    # Only half hour
    if int(hour[2]) not in [0, 3]:
        return "{} must be 0 / 3".format(hour[2])

    if int(hour[3]) != 0:
        return "{} must be 0".format(hour[3])

    return "OK"
