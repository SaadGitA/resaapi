from pathlib import Path
import os
import yaml


class Config:
    @staticmethod
    def get_config():
        # Import config file
        config = str(Path().absolute()) + '/config/config.yml'
        with open(config, "r") as config_file:
            booking_config = yaml.load(config_file)
        return booking_config
